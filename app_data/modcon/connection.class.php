<?php
	require_once(__DIR__.'/../../../config.inc.php');

class Connection
{

	private static $_dbLnk = 'default';
	private static $_dbObj =  null;

	public static function setDbLnk($value)
	{
		self::$_dbLnk = $value;
	}

    protected static function DbConnect() 
	{
		try {
			if ( !isset (self::$_dbObj[self::$_dbLnk]) || self:: $_dbObj[self::$dbLnk] == null) {
				self::$_dbObj[self::$_dbLnk] = new PDO('mysql:host=' . HOSTNAME . ';dbname=' . DATABASE,USERNAME,PASSWORD);
				self::$_dbObj[self::$_dbLnk]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				self::$_dbObj[self::$_dbLnk]->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
			}

			return self::$_dbObj[self::$_dbLnk];

		} catch (PDOException $e) {
			print "Database connection error\n";
			exit();
		}
	}
}

