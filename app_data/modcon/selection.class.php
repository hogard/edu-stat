<?php
require_once('connection.class.php');

class Selection extends Connection
{
	public $results;
	private $sql;

	public function __construct($key, $value, $value2 = null) 
	{
		self::sqlRouter($key, $value, $value2);
	}

	private function sqlRouter($key, $value, $value2) 
	{
		switch ( $key ) {
			case "LZIP":
				$this->sql = 'SELECT NCESID, DIST_NAME FROM nces_dstxpnd 
							INNER JOIN nces_schloc ON NCESID = NCES_ID
							WHERE LZIP = ' . $value . ' GROUP BY DIST_NAME';
				break;
			case "NCESID":
				$this->sql = 'SELECT * FROM nces_dstxpnd 
							INNER JOIN nces_schloc ON NCESID = NCES_ID
							WHERE NCESID = ' . $value . ' GROUP BY DIST_NAME';
				break;
			case "LATLON":
				$this->sql = 'SELECT NCESID, DIST_NAME FROM nces_dstxpnd 
							INNER JOIN nces_schloc ON NCESID = NCES_ID
							WHERE (LATCOD BETWEEN ' . $mnLat . 'AND ' . $mxLat . ') 
							AND   (LONCOD BETWEEN ' . $mnLon . 'AND ' . $mxLon . ')
							GROUP BY DIST_NAME';
			default:
				$this->sql = null;
		}
	}

	public function fetchResults() 
	{
		$sql = $this->sql;

		if ( $sql != null ) {

			$req = parent::DbConnect()->query($sql);
			$req->setFetchMode(PDO::FETCH_ASSOC);

			while ( $row = $req->fetch() ) {
				$this->results[] = $row;
			}

			return $this->results;
		}
	}
}