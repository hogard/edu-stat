<?php

class Preparation extends Selection
{
	public $district;
	private $array;

	public function __construct ($key, $value)
	{
		$result = new Selection($key, $value);
		$this->array = $result->fetchResults()[0];
		self::districtMaker($this->array);
	}

	public function districtMaker($array)
	{
		$this->district = array (
			'ncesid'       => $array['NCESID'], 
			'distname'     => relabel($array['DIST_NAME']),
			'distperpupil' => round(($array['TCURELSC'] * 1000) / ($array['V33'])),
			'elsecbudg'    => $array['TCURELSC'],    
			'enrollment'   => $array['V33'],
			'totrev'	   => (($array['TFEDREV'] + $array['TLOCREV'] + $array['TSTREV']) * 1000),
			'fedrev'       => round($array['TFEDREV'] * 1000),
			'locrev'       => round($array['TLOCREV'] * 1000), 
			'strev'        => round($array['TSTREV'] * 1000),
			'instruction'  => $array['TCURINST'] * 100,
			'supportSrvc'  => $array['TCURSSVC'] * 100, 
			'pupilSupport' => $array['E17'] * 100, 
			'instuSupport' => $array['E07'] * 100, 
			'expndAllAdmn' => ($array['E08'] + $array['E09']) * 100,
			'expndPhyPlnt' => $array['V40'] * 100, 
			'expndTrnsprt' => $array['V45'] * 100, 
			'foodServices' => $array['E11'] * 100,
			'expndBusinss' => $array['V90'] * 100,  
			'entrpriseOps' => $array['V60'] * 100, 
			'expndNonSpec' => $array['V85'] * 100
		);
	}

	public function getDistrict() {
		return($this->district);
	}

}


?>