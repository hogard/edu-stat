<?php
require_once(__DIR__ . '/../resources/includes/display.inc.php');
require_once(__DIR__ . '/../resources/includes/table.inc.php');
?>
<h1 class="framed">Expenditure Per Pupil in <?= $view['distname']; ?>: 
  <span class="highlight">$<?= number_format($view['distperpupil']); ?></span>
  <span id="options_bar"><img src="img/bars.svg" style="height: 24px;"></span>
</h1>

<div id="display_wrap">
   <p class="framed">
  <?= $view['distname']; ?> spent approximately $<?= number_format($view['distperpupil']); ?> for every student enrolled in 2013:</br></br>
	<?= $statecomparison; ?> the state average ($<?= number_format($stateperpupil); ?>) and <?= $fedcomparison; ?> the national average ($<?= number_format($fedperpupil); ?>).</br>
	<?= $distcomparison; ?>
  </p>
</div>
   
<div id="budget_wrap">
  <div>
  Federal:</br>
  <span id="fedval"></br></span></br>
  </br>
  State:</br>
  <span id="staval"></br></span></br>
  </br>
  Local:</br>
  <span id="locval"></br></span></br>
  </div>
  <div id="view_column" value=<?= $view['ncesid']; ?>>
  <p><?= shorten($view['distname']); ?></p>
    <div class="div_column" id="left_column">
      <?= $viewFedDiv; ?>
      <?= $viewStaDiv; ?>
      <?= $viewLocDiv; ?>
    </div>
  </div>
  <?php if ( $comp !== false && $comp['ncesid'] != $view['ncesid']): ?>
  <div id="comp_column" value=<?= $comp['ncesid']; ?>>
  <p><?= shorten($comp['distname']); ?></p>
    <div class="div_column" id="right_column">
      <?= $compFedDiv; ?>
      <?= $compStaDiv; ?>
      <?= $compLocDiv; ?>
    </div>
  <?php endif; ?>
  </div>
</div>

    <table>
      <thead>
        <tr>
          <th width="40%">Expenditures</th>
          <th><?= $tableHead; ?></th>
        </tr>
      </thead>
      <tbody class="table_body">
        <tr>
          <td class="label">Student Instruction</td>
          <td><?= viewOutlay(parseOutlay($view, "instruction"), $comp); ?></td>
        </tr>
        <tr>
          <td>School Maintenance</td>
          <td><?= viewOutlay(parseOutlay($view, "expndPhyPlnt"), $comp); ?></td>
        </tr>
        <tr>
          <td>Administration</td>
          <td><?= viewOutlay(parseOutlay($view, "expndAllAdmn"), $comp); ?></td>
        </tr>
        <tr>
          <td>Pupil Support Services</td>
          <td><?= viewOutlay(parseOutlay($view, "pupilSupport"), $comp); ?></td>
        </tr>
        <tr>
          <td>Staff Support Services</td>
          <td><?= viewOutlay(parseOutlay($view, "instuSupport"), $comp); ?></td>      
        </tr>
        <tr>
          <td>Student Transportation</td>
          <td><?= viewOutlay(parseOutlay($view, "expndTrnsprt"), $comp); ?></td>
        </tr>
        <tr>
          <td>Food Services</td>
          <td><?= viewOutlay(parseOutlay($view, "foodServices"), $comp); ?></td>
        </tr>
        <tr>
          <td>Business Expenditures</td>
          <td><?= viewOutlay(parseOutlay($view, "expndBusinss"), $comp); ?></td>
        </tr>
        <tr>
          <td>Enterprise Operations</td>
          <td><?= viewOutlay(parseOutlay($view, "entrpriseOps"), $comp); ?></td>
        </tr>
        <tr>
          <td>General Expenditures</td>
          <td><?= viewOutlay(parseOutlay($view, "expndNonSpec"), $comp); ?></td>
        </tr>
      </tbody>
    </table>
</div>