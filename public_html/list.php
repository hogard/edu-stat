<?php
require_once(__DIR__ .'/../resources/includes/autoload.inc.php');

$_zip = $_GET['zip'];
$_districts = new Selection('LZIP', $_zip);
$_results = $_districts->fetchResults();

if ( !$_results ) {
?>

</br>
<p>No districts found for this zip code</p>

<?php
} else {
?>

</br>
<p>Select a district or search in a different zip code</p>
<br/>
<div id="radiolist">
  <form>

<?php
foreach ($_results as $district) {
?>

  <input class="radios" type="radio" name="district" value="<?= $district['NCESID'] ?>"> <?= (relabel($district['DIST_NAME'])); ?><br>

<?php
}
?>

  </br>
  <input type="button" class="custbutton" id="getNcesid" value="Submit">
  </form>
</div>

<?php 
} 
?>