<?php require_once ('../resources/includes/autoload.inc.php'); ?>


<div id="content_tabs">
    <div id="search_tab">New Search</div>
    <div id="expand_tab">Expand</div>
    <div id="compare_tab">Compare</div>
    <div id="save_tab">Save</div>
</div>

<?php
		if ( isset($_SESSION['savedDistricts']) ) {
			$districtList = json_decode($_SESSION['savedDistricts'], $assoc = true);
			foreach ($districtList as $district) {
				$ncesid   = $district['ncesid'];
				$distname = shorten($district['distname']);
				$perpupil = number_format($district['distperpupil']);
				echo "<div id='$ncesid' class='savedList'>$distname <span class='highlight'>$$perpupil</span></div>";
			}
	}
?>

