$(document).ready(function(){

//Runs loop of site descriptors in title bar

    var feature = $('#title > div > span');
    var featureIndex = -1;

    function loopFeature() {
        ++featureIndex;
        feature.eq(featureIndex % feature.length)
            .fadeIn(1000)
            .delay(2000)
            .fadeOut(1000, loopFeature);
    }

    loopFeature();

    $('#content').load('search.php');

//If search.php is present, loads list.php on click || enter

        $('#content').delegate("#getZip", "click", loadList);

        $('#content').delegate('#userZip', 'keypress', function(e) {
            if (e.which === 13) { 
            e.preventDefault(); 
            loadList();
            }
        });

        function loadList(){
            $('p').hide();
            var userZip = $('#userZip').val();
            if ( !userZip ) {
                $('#list').html('<p></br>Field cannot be left blank.</br></p>');
            } else if ( userZip.match(/^[0-9]+$/) == null ){
                $('#list').html('<p></br>Entry must contain numbers only.</br></p>');
            } else if ( userZip.length != 5 ){
                $('#list').html('<p></br>Entry must be five digits.</br></p>');
            } else {
                $.ajax({
                    type: "GET",
                    url: 'list.php',
                    data: "zip="+userZip,
                    success: function(data) {
                        $('#list').html(data);
                    }
                });
            }
        }
        
//If list.php is present, creates chart and display graphics

    $('#content').delegate("#getNcesid", "click", function(){
        var userNcesid = $('input[name="district"]:checked').val();

        if ( typeof(userNcesid) != "undefined" && userNcesid !== null ) {
            $.ajax({
                        type: "GET",
                        url: 'display.php',
                        data: "ncesid="+userNcesid,
                        success: function(data) {
                            $('#content').html(data);
                        }
                    });
        } else {
            $('p').html('<p>No district selected.</br></p>');
        }
    });

//Display district revenue data on hover
$('#content').delegate('#view_column', "mouseenter", function() {
        var fedval = $('#view_fed').attr('value');
        var staval = $('#view_sta').attr('value');
        var locval = $('#view_loc').attr('value');
        $('#fedval').hide().html(fedval).fadeIn('slow');
        $('#staval').hide().html(staval).fadeIn('slow');
        $('#locval').hide().html(locval).fadeIn('slow');
    });

$('#content').delegate('#view_column', "mouseleave", function() {
        $('#fedval').html('</br>');
        $('#staval').html('</br>');
        $('#locval').html('</br>');
    });

$('#content').delegate('#comp_column', "mouseenter", function() {
        var fedval = $('#comp_fed').attr('value');
        var staval = $('#comp_sta').attr('value');
        var locval = $('#comp_loc').attr('value');
        $('#fedval').hide().html(fedval).fadeIn('slow');
        $('#staval').hide().html(staval).fadeIn('slow');
        $('#locval').hide().html(locval).fadeIn('slow');
    });

$('#content').delegate('#comp_column', "mouseleave", function() {
        $('#fedval').html('</br>');
        $('#staval').html('</br>');
        $('#locval').html('</br>');
    });

//Expand content menu on click
 $('#content').delegate('#options_bar', "click", function() {
    $('#content').animate({
        'border-top-left-radius': 0, 'border-top-right-radius': 0}, 750);
    var clicks = 0;
    if (clicks == 0) {
        clicks++;
        $('#content_menu').load('displaymenu.php');
        $('#content_menu').animate({height:'4em'}, 500);
    } else {
        $('#content_menu').animate({height:'0em'}, 500);
        clicks--;
    }
});

$('#interface').delegate("#compare_tab", "click", function() {
    $('#content_menu').animate({height:'6em'}, 500);
});

//Save district for comparison
    $('#interface').delegate('#save_tab', "click", function() {
        $.ajax({
                type: "POST",
                url: '../resources/library/session.php',
                data: { "savedArray": districtArray },
                success: function(data) {
                    $('#save_tab').html(data);
                    }
                });
    });

//Load saved district into display table and text on savedList click
    $('#interface').delegate('.savedList', "click", function() {
        var savedNcesid = $(this).attr("id");
         $.ajax({
                type: "GET",
                url: '../resources/library/session.php',
                data: { "savedArray": districtArray },
                success: function(data) {
                    $('#save_tab').html(data);
                    }
                });
         $.ajax({
                        type: "GET",
                        url: 'display.php',
                        data: "ncesid="+savedNcesid,
                        success: function(data) {
                            $('#content').html(data);
                        }
                    });
    });

//Start new search without saving existing district
    $('#interface').delegate('#search_tab', "click", function() {
        delete districtArray;
        location.reload();
    });

});