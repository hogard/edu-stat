<?php require_once ('../resources/includes/autoload.inc.php'); ?>
<!DOCTYPE html>
<html>

<head>
<title>edu.stat</title>
  <link rel="stylesheet" type="text/css" href="css/main.css"> 
  <script src="js/jquery-2.2.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="js/main.js"></script>
</head>

<body>

<div id="title">
  <div>edu.stat<!--
    --><span id="fadingloop">.search</span><!--
    --><span class="fadingloop">.compare</span><!--
    --><span class="fadingloop">.export</span><!--
    --><span class="fadingloop">.research</span>
  </div>
</div>

<div id="interface">
  <div id="content" class="round_div"></div>
  <div id="content_menu" class="round_div"></div>
 </div>

</body>

</html>