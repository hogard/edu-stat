# Edu Stat #

Source of education-statistics web-application project which is available at www.hogard.codes/edustat as of June 2016.

### Description ###

* Single page web-application which allows users to display school district funding sources and expenditures in their local area.
* Submits fields from an html form without refreshing using jQuery and AJAX.
    * Connects to database through data abstraction layer, routing and preparing the appropriate query string.
* Returns a list of potential area districts in a radio-list.
* Loads district data from a relational database, generates graphs and textual breakdown of district funding and expenditures.
* Allows user to save districts for comparison to future districts.
    * Identifiers of saved districts are stored as session variables.
* If two districts are selected for comparison, loads an alternative view that highlights spending categories for each district according to their relationship.

### Currently Under Development ###

* InboundLocator Branch
    * Return highest and lowest spending districts within a twenty mile radius of the district currently selected.
    * Feature is fully functional, but tends to display outlier/anomalous cases.

* StringSearch Branch
    * Form allowing districts to be located by city, county, and region.
    * Suggest/ autocomplete form entry/ display potential districts.
### Set up and Testing ###

* Code is dependent on a relational database, SQL for setup is located at https://hogard@bitbucket.org/hogard/edu-stat-sql-files.git
* Database configuration files can be found in modcon/view/connection.class.php
* Running on PHP version 7.0.4

### Contact ###

* Suggestions for improvement and additional questions/ concerns may be directed to ryanhogard@gmail.com
--