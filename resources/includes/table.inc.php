<?php
include_once('autoload.inc.php');

//prepare home row label
if ( $comp === false ) {
		$tableHead = shorten($districtArray['distname']);
	} else {
		$tableHead = shorten($districtArray['distname'])."</th><th>".shorten($comp['distname'])."</th>";
	}

//prepare outlay category as raw figure and percentage
function parseOutlay($districtArray, $category) {

		$expend = ($districtArray["$category"] / $districtArray['enrollment'])*10;
		$percent = round(( $districtArray["$category"] / $districtArray['elsecbudg'] ), 1);

	return array(
		"category" => $category,
		"expend"  => $expend, 
		"percent" => $percent
		);
}

//display outlay of districts
function viewOutlay($viewArray, $compare) {

	if ( $viewArray['expend'] != 0 ) {
		$viewLine = "$".number_format($viewArray['expend'])." (".$viewArray['percent']."%)";
	} else {
		$viewLine = "";
	}

	if ( $compare === false ) {
		$addendum = '';
	} else {
		$addendum = "</td><td>".matchOutlay($viewArray, parseOutlay($compare, $viewArray['category']))."</td>";
	}

	$statement = $viewLine.$addendum;

	echo $statement;
}


//display outlay of home district, set span color comparison
function matchOutlay($viewArray, $homeArray) {

	if ( $viewArray["expend"] > $homeArray["expend"] ) {
		$expendSpan = 'red';
	} else {
		$expendSpan = 'green';
	}

	if ( $viewArray["percent"] > $homeArray["percent"] ) {
		$percentSpan = 'red';
	} else {
		$percentSpan = 'green';
	}

	if ( $homeArray['expend'] != 0 ) {

		$expendLine = "<span style='color:$expendSpan;'>$".number_format($homeArray['expend'])." </span>(";
		$percentline = "<span style='color:$percentSpan;'>".$homeArray['percent']."%</span>)";
		$statement = $expendLine.$percentline;
	} else {
		$statement = '';
	}

	return $statement;
}

?>
