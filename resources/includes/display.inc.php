<?php
require_once('autoload.inc.php');

if (isset($_SESSION['savedDistricts'])) {
		$_districtList = json_decode($_SESSION['savedDistricts'], $assoc = true);
		$mostRecent = reset($_districtList);
		$compObj = new Preparation('NCESID', $mostRecent['ncesid']);
		$compObj->setDBLnk('comp');
		$comp = $compObj->getDistrict();
	} else {
		$comp = false;
	}

$ncesid = $_GET['ncesid'];
$viewObj = new Preparation('NCESID', $ncesid);
$viewObj->setDBLnk('view');
$view = $viewObj->getDistrict();

		$districtArray = Array( 
			'ncesid' 		=> $view['ncesid'], 
			'distname'     	=> $view['distname'],
			'distperpupil' 	=> $view['distperpupil'],
			'fedrev' 		=> $view['fedrev'],
			'strev' 		=> $view['strev'],
			'locrev' 		=> $view['locrev']
			);

		$jsonDistArray = json_encode($districtArray);

// Label Variables for Display
	$stateperpupil = 9597;
	$fedperpupil = 10652;
	$distperpupil = $view['distperpupil'];
	$statecompare = abs($distperpupil - $stateperpupil);
	$fedcompare = abs($distperpupil - $fedperpupil);

	function compareStatement($foreign, $native) {
		$difference = abs($foreign-$native);
		if ($foreign > $native) {
			return "$".number_format($difference)." less than";
		} else if ($foreign < $native) {
			return "$".number_format($difference)." more than";
		} else {
			return " the same as";
		}
	}

	$statecomparison = compareStatement($stateperpupil, $distperpupil);
	$fedcomparison = compareStatement($fedperpupil, $distperpupil);

	if ( $comp !== false && $comp['ncesid'] != $view['ncesid']) {
		$distcomparison = $districtArray['distname'].' spent '
			.compareStatement($comp['distperpupil'], $distperpupil).' '
			.$comp['distname'].'.</br>';
	} else {
		$distcomparison = '';
	}


//Create div  bar graph parameters

	function divBarSet($array, $key, $type){
		if ($type == "height") {
			return round( $array["$key"] / $array['totrev'] * 196 );
		} else if ($type == "display") {
			if (round( $array["$key"] / $array['totrev'] * 196 ) > 20) {
				return round( $array["$key"] / $array['totrev'] * 100 )."%";
			} else {
				return '';
			}
		} else {
			return round( $array["$key"] / $array['totrev'] * 100 )."%";
		}
	}

	$viewFedDiv = "<div class ='fed_div_bar' id ='view_fed'
		value = '$".number_format($view['fedrev'])."
		</br>".divBarSet($view, 'fedrev', 'value')."'
		style = 'height: ".divBarSet($view, 'fedrev', 'height')."px;
		line-height: ".divBarSet($view, 'fedrev', 'height')."px;'>
		".divBarSet($view, 'fedrev', 'display')."</div>";

	$viewStaDiv = "<div class ='sta_div_bar' id ='view_sta'
		value = '$".number_format($view['strev'])."
		</br>".divBarSet($view, 'strev', 'value')."'
		style= ' height: ".divBarSet($view, 'strev', 'height')."px;
		line-height: ".divBarSet($view, 'strev', 'height')."px;'>
		".divBarSet($view, 'strev', 'display')."</div>";

	$viewLocDiv = "<div class ='loc_div_bar' id ='view_loc'
		value = '$".number_format($view['locrev'])."
		</br>".divBarSet($view, 'locrev', 'value')."'
		style= ' height: ".divBarSet($view, 'locrev', 'height')."px;
		line-height: ".divBarSet($view, 'locrev', 'height')."px;'>
		".divBarSet($view, 'locrev', 'display')."</div>";

	if ( $comp !== false && $comp['ncesid'] != $view['ncesid']) {
		$compFedDiv = "<div class ='fed_div_bar'  id ='comp_fed'
			value = '$".number_format($comp['fedrev'])."
			</br>".divBarSet($comp, 'fedrev', 'value')."'
			style= ' height: ".divBarSet($comp, 'fedrev', 'height')."px;
			line-height: ".divBarSet($comp, 'fedrev', 'height')."px;'>
			".divBarSet($comp, 'fedrev', 'display')."</div>";

		$compStaDiv = "<div class ='sta_div_bar' id ='comp_sta'
			value = '$".number_format($comp['strev'])."
			</br>".divBarSet($comp, 'strev', 'value')."'
			style = 'height: ".divBarSet($comp, 'strev', 'height')."px;
			line-height: ".divBarSet($comp, 'strev', 'height')."px;'>
			".divBarSet($comp, 'strev', 'display')."</div>";

		$compLocDiv = "<div class ='loc_div_bar' id ='comp_loc'
			value = '$".number_format($comp['locrev'])."
			</br>".divBarSet($comp, 'locrev', 'value')."'
			style= ' height: ".divBarSet($comp, 'locrev', 'height')."px;
			line-height: ".divBarSet($comp, 'locrev', 'height')."px;'>
			".divBarSet($comp, 'locrev', 'display')."</div>";
	}
?>

<script type="text/javascript">
	var districtArray = <?= $jsonDistArray; ?>;
</script>