<?php
session_start();

include_once(__DIR__ . '/../library/library.php');

spl_autoload_register('autoloader::modconLoader');
spl_autoload_register('autoloader::viewLoader');

class autoloader
{
	public static function libraryLoader($class_name)
	{
		$path = __DIR__ . '/../resources/library/';
		$class_name = strtolower($class_name);

		include $path.$class_name.'.class.php';
	}


	public static function modconLoader($class_name)
	{
		$path = __DIR__ . '/../../app_data/modcon/';
		$class_name = strtolower($class_name);
		
		include $path.$class_name.'.class.php';
	}

	public static function viewLoader($class_name)
	{
		$path = __DIR__ . '/../../app_data/view/';
		$class_name = strtolower($class_name);

		include $path.$class_name.'.class.php';
	}
}

?>