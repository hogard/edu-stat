<?php
require_once('autoload.inc.php');

if (isset($_COOKIE['savedDistricts'])) {
		$_districtList = json_decode($_COOKIE['savedDistricts'], $assoc = true);
		$compareDistrict = reset($_districtList);
	} else if (isset($_SESSION['districtArray'])) {
		$_districtList = json_decode($_SESSION['savedDistricts'], $assoc = true);
		$compareDistrict = reset($_districtList);
	} else {
		$compareDistrict = false;
	}

$ncesid = $_GET['ncesid'];
$result = new Selection('NCESID', $ncesid);
$district = $result->fetchResults()[0];

$dist_name = $district['DIST_NAME'];
	  $relabeldist = relabel($dist_name);
	  $enrollment = $district['V33'];
	  $dist_expend = 1000 * $district['TCURELSC'];
	  $elsecbudg = $district['TCURELSC'];
	  $totalrev = 1000 * $district['TOTALREV'];
	  $distperpupil = round($dist_expend / $enrollment);
	  $fedrev = round($district['TFEDREV'] * 1000);
	  $strev = round($district['TSTREV'] * 1000);
	  $locrev = round($district['TLOCREV'] * 1000);

	//Lable input fetched for use in graphics, json encoded at end of document
	  		$instruction = $district['TCURINST']*100;
	  			$instrctPrcnt = round((($district['TCURINST'])/($district['TCURELSC']))*100);
	  			$expndInstrct = $district['E13']*100;
	  			$payInstrBenf = $district['J13']*100;
	  			$retirementTr = $district['J12']*100;
	  			$stNonBenInst = $district['V91']*100;
	  			$paymentsPriv = ($instruction-($expndInstrct+$payInstrBenf+$retirementTr+$stNonBenInst));
	  		$supportSrvc = $district['TCURSSVC']*100;
		  		$pupilSupport = $district['E17']*100;
		  		$instuSupport = $district['E07']*100;
		  			$expndGenAdmn = $district['E08']*100;
		  			$expndSchAdmn = $district['E09']*100;
		  		$expndAllAdmn = $expndGenAdmn+$expndSchAdmn;
		  		$expndPhyPlnt = $district['V40']*100;
		  		$expndTrnsprt = $district['V45']*100;
		  		$expndBusinss = $district['V90']*100;
		  		$expndNonSpec = $district['V85']*100;
		  	$otherPrgrms =  $district['TCUROTH']*100;
		  		$foodServices = $district['E11']*100;
		  		$entrpriseOps = $district['V60']*100;
		  		$otherExpend  = ($expndNonSpec + $otherPrgrms - $foodServices + $entrpriseOps);
		  		$ttlSalaries = $district['Z32']*100;
		  		$ttlBenefits = $district['Z34']*100;

		$districtArray = Array( 'ncesid' => $ncesid, 'distname' => $relabeldist,
							'fedrev' => $fedrev, 'locrev' => $locrev, 'strev' => $strev,
							'instruction'  => $instruction, 'expndInstrct'  => $expndInstrct,
							'payInstrBenf' => $payInstrBenf, 'retirementTr' => $retirementTr,
							'stNonBenInst' => $stNonBenInst, 'paymentsPriv' => $paymentsPriv,
							'supportSrvc'  => $supportSrvc, 'pupilSupport'  => $pupilSupport, 
							'instuSupport' => $instuSupport, 'expndGenAdmn' => $expndGenAdmn, 
							'expndSchAdmn' => $expndSchAdmn, 'expndAllAdmn' => $expndAllAdmn,
							'expndPhyPlnt' => $expndPhyPlnt, 'expndTrnsprt' => $expndTrnsprt, 
							'expndBusinss' => $expndBusinss, 'expndNonSpec' => $expndNonSpec, 
							'otherPrgrms'  => $otherPrgrms,	 'entrpriseOps' => $entrpriseOps, 
							'otherExpend'  => $otherExpend,  'foodServices' => $foodServices, 
							'instrctPrcnt' => $instrctPrcnt,   'elsecbudg'  => $elsecbudg,    
							'enrollment'   => $enrollment,   'distperpupil' => $distperpupil);

		$jsonDistArray = json_encode($districtArray);

// Label Variables for Display
	$stateperpupil = 9597;
	$fedperpupil = 10652;
	$statecompare = abs($distperpupil - $stateperpupil);
	$fedcompare = abs($distperpupil - $fedperpupil);

// Label Variables for Display
	$stateperpupil = 9597;
	$fedperpupil = 10652;
	$statecompare = abs($distperpupil - $stateperpupil);
	$fedcompare = abs($distperpupil - $fedperpupil);

	function compareStatement($foreign, $native) {
		$difference = abs($foreign-$native);
		if ($foreign > $native) {
			return "$".number_format($difference)." less than";
		} else if ($foreign < $native) {
			return "$".number_format($difference)." more than";
		} else {
			return " the same as";
		}
	}

	$statecomparison = compareStatement($stateperpupil, $distperpupil);
	$fedcomparison = compareStatement($fedperpupil, $distperpupil);

	if ( $compareDistrict !== false && $compareDistrict['ncesid'] != $districtArray['ncesid']) {
		$distcomparison = $compareDistrict['distname'].' spent '
			.compareStatement($compareDistrict['distperpupil'], $distperpupil).' '
			.$districtArray['distname'].'.</br>';
	} else {
		$distcomparison = '';
	}

?>

<script type="text/javascript">
	var districtArray = <?= $jsonDistArray; ?>;
</script>