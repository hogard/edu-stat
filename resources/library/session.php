<?php
require_once( __DIR__ .'/../includes/autoload.inc.php');

	if (isset($_POST['savedArray'])) {
			$newArray = $_POST['savedArray'];

		if (isset($_SESSION['savedDistricts'])) {
			$districtList = json_decode($_SESSION['savedDistricts'], $assoc = true);

			if (checkDuplicate($districtList, 'ncesid', $newArray) == false) {
				$temporalList = Array($newArray['ncesid'] => $newArray);
				$districtList = $temporalList + $districtList;
				$jsonEncodedArray = json_encode($districtList);
				$_SESSION['savedDistricts'] = $jsonEncodedArray;
			} else {
				$index = array_search($districtList, $newArray);
					unset($districtList["$index"]);
					array_values($districtList);
				$temporalList = Array($newArray['ncesid'] => $newArray);
				$districtList = $temporalList + $districtList;
				$jsonEncodedArray = json_encode($districtList);
				$_SESSION['savedDistricts'] = $jsonEncodedArray;
			}
		} else {
			$districtList = Array($newArray['ncesid'] => $newArray);
			$jsonEncodedArray = json_encode($districtList);
			$_SESSION['savedDistricts'] = $jsonEncodedArray;
		}
	}

	echo "District Saved";
?>