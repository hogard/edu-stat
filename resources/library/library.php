<?php
//Check if for duplicate $array nested in $arrayList based on key value

	function checkDuplicate($arrayList, $key, $array) {
		foreach ($arrayList as $instance)
			if (isset($instance) && $instance[$key] == $array[$key]) 
				return true;
			return false;
	}

// Outputs standardized district names

	function relabel($distlabel) {
	  	$distcap = ucwords(strtolower($distlabel));
	  	$schschool = array(" sch ", " Sch ", " sc ", " Sc ");
	  	$distdistrict = array(" dist", " Dist", " dst", " Dst");
	  	$romnum = array("-ii ", " Ii ", "-iii ", " Iii ", "-iv ", " Iv ");
	  	$romupper = array("-II ", " II ", "-III ", " III ", "-IV ", "IV ");
	  		$schcatch = str_replace($schschool, " School ", $distcap);
	  			if(strpos("x".$schcatch, 'District') == false) {
	  				$distcatch = str_replace($distdistrict, " District ", $schcatch);
	  			} else {
	  				$distcatch = $schcatch;
	  			}
	  		$romcatch = str_replace($romnum, $romupper, $distcatch);
	  	return $romcatch;
	}

//Outputs readable labeled numbers

	function readable($n) {
        $n = (0+str_replace(",","",$n));
        
        // Check to ensure int
        if(!is_numeric($n)) return false;
   
        if($n>1000000000000) return round(($n/1000000000000),1).' trillion';
        else if($n>1000000000) return round(($n/1000000000),1).' billion';
        else if($n>1000000) return round(($n/1000000),1).' million';
        else if($n>1000) return round(($n/1000),1).' thousand';
        
        return number_format($n);

        $english_format_number = number_format($number);
    }

//Keep first two words of string for table display
	function shorten ($district) {
	    $district = str_replace("School", "", $district);
	    $district = str_replace("District", "", $district);

	    $pieces = explode(" ", $district);
		$district = implode(" ", array_splice($pieces, 0, 2));
			if ( strlen($district) > 12 ) {
				$pieces = explode(" ", $district);
				$district = implode(" ", array_splice($pieces, 0, 1));
			}

		return $district;
		}


?>